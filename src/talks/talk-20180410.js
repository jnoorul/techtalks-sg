export const talk = {
  "title": "Progressive Web Applications",
  "presenter": "NoorulAmeen / Ritesh Mehrotra",
  "questions": [
    {
      "question": "How would you rate the content of the talk?",
      "answer": ["Poor", "Average", "Good", "Excellent"]
    },
    {
      "question": "How would you rate the speaker?",
      "answer": ["Poor", "Average", "Good", "Excellent"]
    },
    {
      "question": "How would you rate the venue?",
      "answer": ["Poor", "Average", "Good", "Excellent"]
    }
  ]
};