import styled from 'styled-components';

export const H1 = styled.h1`
  font-size: 4vw;
`;

export const H2 = styled.h1`
  font-size: 3vw;
`;

export const H3 = styled.h1`
  font-size: 2vw;
`;