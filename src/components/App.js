import React, { Component } from 'react';
import './App.css';
import { H1, H2 } from './common/Typography';
import {talk} from '../talks/talk-20180410';
import logo from '../assets/techtalks-logo.jpeg';

class App extends Component {
  render() {
    return (
      <div className="appShell">
        <div className="header">
          <div className="headerBanner">
            <img src={logo} alt="logo" width="100%" height="100%" />
          </div>
        </div>
        <div id="mainContent">
          <H1>
            {talk.title}
          </H1>
          <H2 style={{textAlign: 'center'}}>
            By {talk.presenter}
          </H2>
          <br/> <br/><br/> <br/>
        </div>
        <div class="footer">
          Copyright © 2018 Tech Talks.
        </div>
      </div>
    );
  }
}

export default App;
